# Generated by Django 5.0 on 2023-12-15 02:09

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("receipts", "0002_alter_receipt_account"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="account",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.account",
            ),
        ),
    ]
